#!/bin/bash

song="${1}"
speed="${2}"

trap ctrl_c INT

function ctrl_c() {
  screen -S tidal -p 0 -X stuff "hush^M" > /dev/null 2>&1
  exit
}

cp seedstate."${song}" current_state_gameoflife
cp start_state_tidalcycles current_state_tidalcycles
cp instruments."${song}" current_instruments || echo "No instruments file found for song."

while true; do
  cat current_state_gameoflife
  command=':{\ndo\n'
  while read line; do
    screen -S tidal -p 0 -X stuff "${line}\n" > /dev/null 2>&1
  done < current_state_tidalcycles

  screen -S tidal -p 0 -X stuff "^M" > /dev/null 2>&1

  python3 iterate.py
  sleep "${speed}"
  clear
  #echo; echo;
  #echo "Next step..."
done

