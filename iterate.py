import numpy

def main():
    current_state = numpy.loadtxt('current_state_gameoflife', dtype=int)

    # Write the new TidalCycles state to inject into GHCI (outside this script)
    ## By first reading the instruments file to determine how the TidalCycles pattern should sound
    open('current_state_tidalcycles', 'w').close()
    beats = []
    effects = []
    with open('current_instruments') as f:
        for line in f:
            # Directly pass through lines starting @ as Haskell GHCI code directives (e.g. let, or another pattern unaltered)
            if line.startswith('@') and line[1].isdigit():
                with open("current_state_tidalcycles", "a") as f:
                    f.write(line[1:])
            else:
                split_line = line.strip().split('#', 1)
                beats.append(split_line[0].split('  '))
                if len(split_line) > 1:
                    effects.append('#' + split_line[1])
                else:
                    effects.append("")

    ## Then building the TidalCycles patterns based on whether cells are alive or dead
    ### By taking the number of beats modulo the grid row length
    ### so that we can have dynamic sized instrument sets and grid sizes
    rows = current_state.shape[0]
    columns = current_state.shape[1]
    # expose global cps parameter?
    for row in range(0, rows):
        grid_row_modulo_instruments_rows = row % len(beats)
        pattern_string_prefix = "once $ s \"" # could also support user-defined prefix, even per row, or global - same for suffix, or otherwise expose palindrome toggle
        pattern_string_suffix = "\" " + effects[grid_row_modulo_instruments_rows] + "\n"
        pattern_string_middle = ""
        for column in range(0, columns):
            grid_column_modulo_instruments_row_length = column % len(beats[grid_row_modulo_instruments_rows])
            # wow - instead of sticking to 8 beats per bar we accidentally forgot and got a number of beats per bar equivalent to the number of cells alive on that row
            # but that could seriously be worth sticking with as the long-term direction - or instead expose the capability to be dynamic or use-define beats per bar (even per row!)
            pattern_string_middle += beats[grid_row_modulo_instruments_rows][grid_column_modulo_instruments_row_length] + " " if current_state[row, column] == 1 else "~ " 
            # could also try sustain/elongate for empty cells - export as toggle
        with open("current_state_tidalcycles", "a") as f:
            f.write(pattern_string_prefix + pattern_string_middle + pattern_string_suffix)

    # Write the new Game of Life state for the next iteration (outside this script)
    ## By calculating immediate neighbours on a torus grid via conditional modulo arithmetic on the dimensions
    next_state = numpy.zeros((current_state.shape[0], current_state.shape[1]))
    for row, column in numpy.ndindex(current_state.shape):
        # Indices for torus grid wrapping calculations
        rowMinusOne = (current_state.shape[0]-1 if row-1 < 0 else row-1)
        rowPlusOne = (0 if row+1 >= current_state.shape[0] else row+1)
        columnMinusOne = (current_state.shape[1]-1 if column-1 < 0 else column-1)
        columnPlusOne = (0 if column+1 >= current_state.shape[1] else column+1)
        neighbours_alive = (1 if current_state[rowMinusOne][columnMinusOne] == 1 else 0) \
            + (1 if current_state[rowPlusOne][columnMinusOne] == 1 else 0) \
            + (1 if current_state[rowMinusOne][columnPlusOne] == 1 else 0) \
            + (1 if current_state[rowPlusOne][columnPlusOne] == 1 else 0) \
            + (1 if current_state[row][columnMinusOne] == 1 else 0) \
            + (1 if current_state[row][columnPlusOne] == 1 else 0) \
            + (1 if current_state[rowMinusOne][column] == 1 else 0) \
            + (1 if current_state[rowPlusOne][column] == 1 else 0)
        if current_state[row, column] == 1 and neighbours_alive < 2 or neighbours_alive > 3:
            pass # print("cell is about to die - placeholder for if I want to use this later")
        elif (current_state[row, column] == 1 and 2 <= neighbours_alive <= 3) or (current_state[row, column] == 0 and neighbours_alive == 3):
            next_state[row, column] = 1

    ## Then writing next state file to disk
    with open('current_state_gameoflife','wb') as f:
        for line in numpy.matrix(next_state):
            numpy.savetxt(f, line, fmt='%d')

if __name__ == "__main__":
    main()
