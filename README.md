### LifeCycles

LifeCycles uses the game of life cellular automata patterns to drive TidalCycles patterns.  

### Usage

Open two terminals.  

In the first terminal type: `./first.sh`  

In the second terminal type: `./second.sh SONGNAME SPEED`  

For example:  
`./second.sh birdsong 2`  
`./second.sh fun1 1`  
`./second.sh fun2 1`  
`./second.sh gliders 0.5625`


You can further customise instruments by copying and modifying an instruments file. It TidalCycles mini notation and supports most of it - technical limitations prevent SOME more advanced aspects such as elongation. If you get it wrong you will see syntax errors in the first terminal.  

You can also customise the seed pattern by copying one you like or the zero pattern and then altering which cells are alive to start.  

It is an 8 by 8 grid currently but supports surprising diversity. Look here for some inspiration or just try random chaos for cell placement: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Examples_of_patterns  

#### Depends on
tidalcycles  
screen (apt install screen)  
python3   
numpy (pip3 install numpy)  

### Future work
- I want to try different grid sizes and mappings.  
- e.g. 8 by 8 but with 4 beats to a bar (chord or repeat to get from 4 to 8? or use the second half of one dimension for effects/params?)  
- or 12 by 12 with 12 patterns and 8 beats per bar and 4 effects/params? or 12 by 8? or 8 by 12?  
- Whatever happens, any increase above 8 by 8 is a big explosion in the size of the state space, which could be good - certainly allows bigger patterns to fit, such as bigger spaceships or oscillators.  
- Support elongation mini notation with a check to replace with skips if there is no previous beat to elongate, to avoid a syntax error.  
- Write Dockerfile 


- Allow selection of Game of Life or Day and Night: https://en.wikipedia.org/wiki/Day_and_Night_(cellular_automaton)

- Explore other lifelike CAs: https://en.wikipedia.org/wiki/Life-like_cellular_automaton 

- Explore HighLife: https://en.wikipedia.org/wiki/Highlife_(cellular_automaton)

- Cellular Orchestra: run multiple copies of lifecycle at once so that different instances can combine - perhaps some dynamic beats per bar, some user-defined, optionally with different grid sizes and instruments sets
